#undef HAVE_ESD

#undef HAVE_LC_MESSAGES
#undef HAVE_STPCPY
#undef PACKAGE
#undef VERSION
#undef ENABLE_NLS
#undef HAVE_CATGETS
#undef HAVE_GETTEXT
#undef HAVE_LINUX_SOUNDCARD_H
#undef HAVE_PROGRAM_INVOCATION_SHORT_NAME
#undef HAVE_PROGRAM_INVOCATION_NAME

/* Define if you have the Xxf86dga library (-lXxf86dga).  */
#define HAVE_LIBXXF86DGA 1

/* Define if you have the Xxf86vm library (-lXxf86vm).  */
#define HAVE_LIBXXF86VM 1

