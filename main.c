#include <gnome.h>
#include <gdk/gdkx.h>
#define GTKTV_PRIVATE
#include "gtktv.h"


/* This points to our toplevel window */
GtkWidget *app;

static void
quit_cb (GtkWidget *widget, void *data)
{
	gtk_main_quit ();

	return;
}

static void
prepare_app()
{
	GtkWidget *tv;
	GdkGC *gc;
	Atom xv_freq;
	Atom xv_encoding;

	xv_freq = XInternAtom(GDK_DISPLAY(), "XV_FREQ", False);
	xv_encoding = XInternAtom(GDK_DISPLAY(), "XV_ENCODING", False);
	
	app = gnome_app_new ("gtv", " Gnomovision");
	gtk_widget_realize (app);
	gtk_signal_connect (GTK_OBJECT (app), "delete_event",
			    GTK_SIGNAL_FUNC (quit_cb),
			    NULL);
	
	tv = gtk_tv_new ();
	gnome_app_set_contents (GNOME_APP (app), tv);

	gtk_widget_show(tv);
	gtk_widget_show(GTK_WIDGET(app));
	g_print("Ready to fire up the PutVideo\n");
#if 1
	g_print("Port: %d\n", 36);
	gc = gdk_gc_new((GTK_WIDGET(tv))->window);
	g_print("GC: %X GdkWindow: %X\n", gc, GTK_WIDGET(tv)->window);
	XvSetPortAttribute(GDK_DISPLAY(), 36, xv_encoding, 1); 
	XvSetPortAttribute(GDK_DISPLAY(), 36, xv_freq, 980);
	gtk_tv_video_start_stop(GTK_TV(tv), TRUE);

#endif
}

int
main(int argc, char *argv[])
{
	gnome_init ("gtktv-test", "1", argc, argv);

	prepare_app ();

	gtk_main ();
	
	return 0;
}

