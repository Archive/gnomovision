/* GtkTV - GTK+ Widget for interfacing with the XVideo X extension
 *
 * Copyright (C) 2000 Mark Crichton
 *
 * Authors: Mark Crichton <crichton@gimp.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __GTK_TV_H__
#define __GTK_TV_H__

#include <gdk/gdk.h>
#include <gtk/gtkwidget.h>
#include <X11/extensions/Xv.h>
#include <X11/extensions/Xvlib.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define GTK_TYPE_TV		(gtk_tv_get_type())
#define GTK_TV(obj)		(GTK_CHECK_CAST ((obj), GTK_TYPE_TV, GtkTV))
#define GTK_TV_CLASS(klass)	(GTK_CHECK_CLASS_CAST ((klass), \
				 GTK_TYPE_TV, GtkTVClass))
#define GTK_IS_TV(obj)		(GTK_CHECK_TYPE ((obj), GTK_TYPE_TV))

typedef struct _GtkTV GtkTV;
typedef struct _GtkTVClass GtkTVClass;
typedef struct _GtkTVPrivate GtkTVPrivate;

enum _GtkTVAttrib {
  GTK_TV_ATTRIB_ENCODING = (1 << 0),
  GTK_TV_ATTRIB_COLOR = (1 << 1),
  GTK_TV_ATTRIB_HUE = (1 << 2),
  GTK_TV_ATTRIB_SATURATION = (1 << 3),
  GTK_TV_ATTRIB_BRIGHTNESS = (1 << 4),
  GTK_TV_ATTRIB_CONTRAST = (1 << 5),
  GTK_TV_ATTRIB_FREQ = (1 << 6),
  GTK_TV_ATTRIB_MUTE = (1 << 7),
  GTK_TV_ATTRIB_VOLUME = (1 << 8),
  GTK_TV_ATTRIB_COLORKEY = (1 << 9)
};
typedef enum _GtkTVAttrib GtkTVAttrib;
  
struct _GtkTV
{
  GtkWidget parent_widget;
  GtkTVPrivate *priv;
};

struct _GtkTVClass
{
  GtkWidgetClass parent_class;
};

guint       gtk_tv_get_type (void);
GtkWidget*  gtk_tv_new (void);
void        gtk_tv_set_freq (guint freq);
void        gtk_tv_observe_aspect_ratio (GtkTV *tv, gboolean bool);
void        gtk_tv_video_start_stop (GtkTV *tv, gboolean bool);
gboolean    gtk_tv_audio_mute (GtkTV *tv, gboolean bool);
gboolean    gtk_tv_set_attribute (GtkTV *tv, gchar *attrib, gint value);
gint        gtk_tv_get_attribute (GtkTV *tv, gchar *attrib);
GList*      gtk_tv_get_port_list (GtkTV *tv);
gboolean    gtk_tv_set_port (GtkTV *tv, gint port);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __GTK_TV_H__ */
