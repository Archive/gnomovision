/* GtkTV - GTK+ Widget for interfacing with the XVideo X extension
 *
 * Copyright (C) 2000 Mark Crichton
 *
 * Authors: Mark Crichton <crichton@gimp.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <gtk/gtk.h>
#include <gdk/gdkx.h>
#include <X11/Xlib.h>
#include <X11/Xatom.h>

#ifdef HAVE_MITSHM
#include <sys/ipc.h>
#include <sys/shm.h>
#include <X11/extensions/XShm.h>
#endif

#include <X11/extensions/Xv.h>
#include <X11/extensions/Xvlib.h>

#include "gtktv.h"

struct _GtkTVPrivate {
  GHashTable *encodings;

  XvEncodingInfo *ei;
  XvAttribute *at;

  gint attrib;
  Atom xv_encoding;
  Atom xv_color;
  Atom xv_hue;
  Atom xv_saturation;
  Atom xv_brightness;
  Atom xv_contrast;
  Atom xv_freq;
  Atom xv_mute;
  Atom xv_volume;
  Atom xv_colorkey;

  gboolean xv_hw_scale;
};

static GtkWidgetClass *parent_class = NULL;

static struct _enc_map {
  gint norm;
  gint input;
  gint encoding;
} *enc_map;

static void gtk_tv_class_init(GtkTVClass * class);
static void gtk_tv_init(GtkTV * tv);
static void gtk_tv_realize(GtkWidget * widget);
static void gtk_tv_size_request(GtkWidget * widget, GtkRequisition * req);
static void gtk_tv_size_allocate(GtkWidget * widget, GtkAllocation * allo);
static gint gtk_tv_destroy(GtkWidget * widget, GdkEventAny * event);

guint gtk_tv_get_type(void)
{
  static guint tv_type = 0;
  
  if (!tv_type) {
    GtkTypeInfo tv_info = {
      "GtkTV",
      sizeof(GtkTV),
      sizeof(GtkTVClass),
      (GtkClassInitFunc) gtk_tv_class_init,
      (GtkObjectInitFunc) gtk_tv_init,
      (GtkArgSetFunc) NULL,
      (GtkArgGetFunc) NULL,
      (GtkClassInitFunc) NULL
    };
    
    tv_type = gtk_type_unique(gtk_widget_get_type(), &tv_info);
  }
  
  return tv_type;
}

static void gtk_tv_class_init(GtkTVClass * class)
{
  GtkWidgetClass *widget_class;
  GtkEventBoxClass *event_class;
  
  widget_class = (GtkWidgetClass *) class;
  event_class = (GtkEventBoxClass *) class;
  
  widget_class->destroy_event = gtk_tv_destroy;
  widget_class->size_request = gtk_tv_size_request;
  widget_class->size_allocate = gtk_tv_size_allocate;
  widget_class->realize = gtk_tv_realize;
}

static void gtk_tv_init(GtkTV * tv)
{
  tv->private = g_new(GtkTVPrivate, 1);
}


GtkWidget *gtk_tv_new(gboolean hwscale, gint port)
{
  /* We have a LOT of initialization to do...
   * This code is basically a copy of xv.c from xawtv.
   */
  
  GtkTV *tv;
  gint i;
  gint ver, rel, req, ev, err, debug;
  gint adaptors, encodings, attributes;
  gint formats;
  guint im_format;
  gint vi_port = -1, vi_adaptor = -1;
  gint im_adaptor =-1, im_port = -1;
  gint dpy = GDK_DISPLAY();
  gchar norm[32], input[32];

  XvAdaptorInfo *ai;
  GtkTVPrivate *priv = g_new0(GtkTVPrivate, 1);
        
  tv = gtk_type_new(gtk_tv_get_type());
  tv->priv = priv;
  
  if (Success != XvQueryExtension(dpy, &ver, &rel, &req, &ev, &err)) {
    g_error("gtkTV: Xv Extension not supported");
    return NULL;
  }
  
  if (Success != XvQueryAdaptors(dpy, GDK_ROOT_WINDOW(), &adaptors, &ai)) {
    g_error("gtkTV: XvQueryAdaptors failed");
    return NULL;
  }
  
  /* Now iterate over all adaptors and all ports in adaptors, filling in
   * information as needed.
   * This will use the last video in port found.
   */
  
  for (i = 0; i < adaptors; i++) {
    if ((ai[i].type & XvInputMask) &&
	(ai[i].type & XvVideoMask) &&
	(vi_port == -1)) {
      if ((ai[i].base_id == port)
	  || (0 == port)) {
	vi_port = ai[i].base_id;
	vi_adaptor = i;
      }
    }
    
    /* Look for HW scaling */
    
    if ((ai[i].type & XvInputMask) &&
	(ai[i].type & XvImageMask) &&
	(im_port == -1)) {
      im_port = ai[i].base_id;
      im_adaptor = i;
    }
  }
  
  if (vi_port == -1) {
    g_error("gtktv: No video ports found!");
    return NULL;
  }
  
  if (Success != XvQueryEncodings (dpy, vi_port, &encodings, &priv->ei)) {
    g_error("gtkTV: XvQueryEncodings failed");
    return NULL;
  }
  
  enc_map = g_malloc(sizeof(*enc_map) * encodings);
  for (i = 0; i < encodings; i++) {
    if (2 == sscanf(priv->ei[i].name, "%31[^-]-%31s", norm, input)) {
      enc_map[i].norm = foo;
      enc_map[i].input = foo;
      enc_map[i].encoding = priv->ei[i].encoding_id;
    } else {
      g_warning("gtkTV: cannot parse encodings");
    }
  }
  
  priv->at = XvQueryPortAttributes(dpy,vi_port,&attributes);
  priv->attrib = 0;

  for (i = 0; i < attributes; i++) {
    if (0 == strcmp("XV_ENCODING",priv->at[i].name)) {
      priv->attrib |= GTK_TV_ATTRIB_ENCODING;
      priv->xv_encoding   = XInternAtom(dpy, "XV_ENCODING", False);
    }
    if (0 == strcmp("XV_COLOR",priv->at[i].name)) {
      priv->attrib |= GTK_TV_ATTRIB_COLOR;
      priv->xv_color      = XInternAtom(dpy, "XV_COLOR", False);
    }
    if (0 == strcmp("XV_HUE",priv->at[i].name)) {
      priv->attrib |= GTK_TV_ATTRIB_HUE;
      priv->xv_hue        = XInternAtom(dpy, "XV_HUE", False);
    }
    if (0 == strcmp("XV_SATURATION",priv->at[i].name)) {
      priv->attrib |= GTK_TV_ATTRIB_SATURATION;
      priv->xv_saturation = XInternAtom(dpy, "XV_SATURATION", False);
    }
    if (0 == strcmp("XV_BRIGHTNESS",priv->at[i].name)) {
      priv->attrib |= GTK_TV_ATTRIB_BRIGHTNESS;
      priv->xv_brightness = XInternAtom(dpy, "XV_BRIGHTNESS", False);
    }
    if (0 == strcmp("XV_CONTRAST",priv->at[i].name)) {
      priv->attrib |= GTK_TV_ATTRIB_CONTRAST;
      priv->xv_contrast   = XInternAtom(dpy, "XV_CONTRAST", False);
    }
    if (0 == strcmp("XV_FREQ",priv->at[i].name)) {
      priv->attrib |= GTK_TV_ATTRIB_FREQ;
      priv->xv_freq       = XInternAtom(dpy, "XV_FREQ", False);
    }
    if (0 == strcmp("XV_MUTE",priv->at[i].name)) {
      priv->attrib |= GTK_TV_ATTRIB_MUTE;
      priv->xv_mute       = XInternAtom(dpy, "XV_MUTE", False);
    }
    if (0 == strcmp("XV_VOLUME",priv->at[i].name)) {
      priv->attrib |= GTK_TV_ATTRIB_VOLUME; 
      priv->xv_volume     = XInternAtom(dpy, "XV_VOLUME", False);
    }
    if (0 == strcmp("XV_COLORKEY",priv->at[i].name)) {
      priv->attrib |= GTK_TV_ATTRIB_COLORKEY;
      priv->xv_colorkey   = XInternAtom(dpy, "XV_COLORKEY", False);
    }
  }
  
  /* look for a YUY2 XvImage */
  
  if (!hwscale) {
    /* Nothing, we just don't use hw scaling */
  } else if (im_port == -1) {
    g_warning("gtkTV: no usable hw scaler found");
  } else {
    fo = XvListImageFormats(dpy, im_port, &formats);
    for(i = 0; i < formats; i++) {
      if (0x32595559 == fo[i].id) {
	im_format = fo[i].id;
	priv->xv_hw_scale = TRUE;
      }
    }
  }
  return GTK_WIDGET(tv);
}

static 
gint gtk_tv_destroy(GtkWidget * widget, GdkEventAny * event)
{
  return FALSE;
}

static 
void gtk_tv_size_request(GtkWidget * widget, GtkRequisition * req)
{
  req->width = 320;
  req->height = 240;
}

static void gtk_tv_size_allocate(GtkWidget * widget, GtkAllocation * allo)
{
}

GdkFilterReturn
gtk_tv_filter(GdkXEvent * gdk_xevent, GdkEvent * event, gpointer data)
{
  XEvent *xevent;
  XvVideoNotifyEvent *xve;
  XvPortNotifyEvent *xpe;
  GdkFilterReturn ret_val;
  
  xevent = (XEvent *) gdk_xevent;
  
  switch (xevent->type) {
  case XvVideoNotify:
    *xve = (XvVideoNotifyEvent *) & xevent;
    g_print("XvPortNotify: reason: %s\n", reasons[xve->reason]);
    ret_val = GDK_FILTER_REMOVE;
    break;
  case XvPortNotify:
    *xpe = (XvPortNotifyEvent *) & xevent;
    g_print("XvPortNotify: %s=%ld\n",
	    XGetAtomName(GDK_DISPLAY(), xpe->attribute), xpe->value);
    ret_val = GDK_FILTER_REMOVE;
    break;
  default:
    ret_val = GDK_FILTER_CONTINUE;
  }
  return (ret_val);
}

static void gtk_tv_realize(GtkWidget * widget)
{
  /* Must add GtkLayout event_filter here for visibility_notify_event! */
  
  GdkWindowAttr attrib;
  gint attrib_mask;
  
  g_return_if_fail(widget != NULL);
  g_return_if_fail(GTK_IS_TV(widget));
  
  GTK_WIDGET_SET_FLAGS(widget, GTK_REALIZED);
  
  attrib.x = widget->allocation.x;
  attrib.y = widget->allocation.y;
  attrib.width = widget->allocation.width;
  attrib.height = widget->allocation.height;
  attrib.wclass = GDK_INPUT_OUTPUT;
  attrib.window_type = GDK_WINDOW_CHILD;
  attrib.event_mask = gtk_widget_get_events(widget) |
    GDK_EXPOSURE_MASK | GDK_BUTTON_PRESS_MASK |
    GDK_BUTTON_RELEASE_MASK;
  attrib.visual = gtk_widget_get_visual(widget);
  attrib.colormap = gtk_widget_get_colormap(widget);
  
  attrib_mask = GDK_WA_X | GDK_WA_Y | GDK_WA_VISUAL | GDK_WA_COLORMAP;
  widget->window = gdk_window_new(widget->parent->window, 
				  &attrib, attrib_mask);
  
  widget->style = gtk_style_attach(widget->style, widget->window);
  
  gdk_window_set_user_data(widget->window, widget);
  gtk_style_set_background(widget->style, widget->window,
			   GTK_STATE_ACTIVE);
  
  gdk_window_add_filter(widget->window, gtk_tv_filter, NULL);
  
  return;
}

gboolean
gtk_tv_video_start_stop(GtkTV *tv, gboolean bool)
{
  gint w, h;
  GdkGC *gc;
  
  if ((TVP(tv)->cur_port) == -1) {
    g_warning("GtkTV: video_start_stop called with uninitialized port"); 
    return FALSE;
  }
  
  
  if (bool) {
    w = GTK_WIDGET(tv)->allocation.width;
    h = GTK_WIDGET(tv)->allocation.height;
    gc = gdk_gc_new(GTK_WIDGET(tv)->window);
    
    XvPutVideo(GDK_DISPLAY(),
	       TVP(tv)->cur_port,
	       GDK_WINDOW_XWINDOW(GTK_WIDGET(tv)->window),
	       GDK_GC_XGC(gc), 0, 0, w, h, 0, 0, w, h);
    
    gdk_gc_destroy(gc);
    
    TVP(tv)->vid_on = TRUE;
    
  } else {
    XvStopVideo(GDK_DISPLAY(), TVP(tv)->cur_port,
		GDK_WINDOW_XWINDOW(GTK_WIDGET(tv)->window));
    TVP(tv)->vid_on = FALSE;
  }
  return TRUE;
}

gboolean
gtk_tv_audio_mute(GtkTV *tv, gboolean bool)
{
}

gboolean
gtk_tv_set_attribute(GtkTV *tv, gchar *attrib, gint value)
{
}

gint
gtk_tv_get_attribute(GtkTV *tv, gchar *attrib)
{
}

GList*
gtk_tv_get_port_list (GtkTV *tv)
{
  return NULL;
}

gboolean
gtk_tv_set_port (GtkTV *tv, gint port)
{
  return TRUE;
}
